#include <iostream>
#include <cstddef>
#include "dbg.h"
#include <yaml-cpp/yaml.h>
#include <cpr/cpr.h>

int main(int argc, char* argv[]) {
    dbg("entry");
    dbg(std::vector<std::string>{argv, argv + argc});

    {
        YAML::Emitter out;
        out << YAML::BeginSeq;
        out << "eggs";
        out << "bread";
        out << "milk";
        out << YAML::EndSeq;
        dbg(out.c_str());
    }

    {
        auto r = cpr::Get(cpr::Url{"http://example.com"});
        dbg(r.status_code);
        dbg(r.header["content-type"]);
        dbg(r.text.size());
    }
    return EXIT_SUCCESS;
}
