# vim: set filetype=python sts=4 ts=4 sw=4 expandtab tw=100 cc=+1:

# https://docs.conan.io/en/latest/reference/conanfile.html
# https://docs.conan.io/en/latest/reference/conanfile/attributes.html
# https://docs.conan.io/en/latest/reference/conanfile/methods.html
# https://docs.conan.io/en/latest/reference/conanfile/other.html

import conans
import os
import logging
import sys

logging.basicConfig(level=logging.INFO, stream=sys.stderr,
    datefmt="%Y-%m-%dT%H:%M:%S",
    format=("%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
        "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"))


class MyConan(conans.ConanFile):
    name = "aucampia-template-conan"
    version = "1.0.0-r0"
    settings = "os", "compiler", "build_type", "arch"
    requires = (
        "yaml-cpp/0.6.3",
        "fmt/6.1.2",
        "cpr/1.3.0",
    )

    exports_sources = ( "*", "!build/*" )
    generators = (
        "scons",
        "cmake",
        "cmake_multi",
        "cmake_paths",
        "cmake_find_package",
        "txt",
        "virtualenv",
        "virtualbuildenv",
        "virtualrunenv",
        "gcc",
        "compiler_args",
        "make",
        "json",
        "pkg_config",
    )
    options = {
        "shared": [True, False],
        "fPIC": [True, False],
    }
    default_options = {
        "shared": False,
        "fPIC": True,
    }
    def build(self):
        logging.info("self.source_folder = %s", self.source_folder)
        logging.info("self.build_folder = %s", self.build_folder)
        logging.info("os.getcwd() = %s", os.getcwd())

        cmake = conans.CMake(self)
        cmake.verbose = True
        cmake.configure()
        cmake.build()
        with conans.tools.environment_append({"BOOST_TEST_LOG_LEVEL": "all", "ARGS": "--verbose"}):
            cmake.test()
