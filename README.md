# aucampia/template/cmake

[![maintenance: sprodadic](https://img.shields.io/badge/maintenance-sporadic-red.svg)](.)

```bash
make -f tool.mf conan-install
make -f tool.mf conan-run-main
make -f tool.mf conan-cmake-run-main run_args="a b c"
```

```bash
make -f tool.mf cmake-build-main
make -f tool.mf cmake-run-main
make -f tool.mf clean
```

## ...

 - https://github.com/cginternals/cmake-init
 - https://github.com/Paspartout/BoilerplatePP
