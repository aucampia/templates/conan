# Script ...

set(BUILD_DIR "build")

find_program(CONAN_CMD "conan" NAMES conan conan.exe conan.cmd conan.bat conan.ps1)
set(CONAN_ARGS)
set(CONAN_BUILD_FOLDER "${BUILD_DIR}/conan")

#add_executable(local::conan IMPORTED)
#set_target_properties(local::conan PROPERTIES IMPORTED_LOCATION "${CONAN_CMD}")
#add_custom_target(conan-install
#    COMMAND local::conan ARGS ${CONAN_ARGS} install "--install-folder=${CONAN_BUILD_FOLDER}" "--build=outdated" "--settings build_type=Debug")

function(conan_install)
    message(STATUS "will run conan install")
    execute_process(COMMAND
        ${CONAN_CMD} ${CONAN_ARGS} install "--install-folder=${CONAN_BUILD_FOLDER}" "--build=outdated" "--settings" "build_type=Debug" ".")
endfunction()

function(conan_clean)
    message(STATUS "removing ${CONAN_BUILD_FOLDER}")
    file(REMOVE_RECURSE ${CONAN_BUILD_FOLDER})
endfunction()

function(handle_target)
    message(STATUS "TOOL_TARGET=${TOOL_TARGET}")

    if(${TOOL_TARGET} MATCHES conan_install)
        conan_install()
    elseif(${TOOL_TARGET} MATCHES conan_clean)
        conan_clean()
    else()
        message(STATUS "Unhandled TOOL_TARGET ${TOOL_TARGET}")
    endif()
endfunction()

handle_target()
